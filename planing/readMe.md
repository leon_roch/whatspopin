# Communication Web App

- [Communication Web App](#communication-web-app)
  - [Idea](#idea)
  - [Use Cases](#use-cases)
  - [Use Case Diagram](#use-case-diagram)
  - [Sketches](#sketches)
  - [System Architecture](#system-architecture)
  - [Functional Sketch](#functional-sketch)
  - [Data Model](#data-model)
  - [API Documentation](#api-documentation)
    - [auth](#auth)
      - [login / register user](#login--register-user)
    - [users](#users)
      - [query users](#query-users)
    - [history](#history)
      - [get user history](#get-user-history)
    - [chats](#chats)
      - [get chat](#get-chat)
      - [create chat](#create-chat)
    - [messages](#messages)
      - [send message](#send-message)

## Idea

I wanna make something like WhatsApp, but it's gonna be called "WhatsPoppin". It'll let people send and get text messages easily, just like chatting with friends.

## Use Cases

- UC01: As a user, I want to be able to register for an account so that I can use the app.*
- UC02: As a user, I want to be able to log in to my account so that I can use the app.*
- UC03: As a user, I want to be able to find users so that I can chat with them.
- UC04: As a user, I want to be able to send messages to other users so that I can chat with them.
- UC05: As a user, I want to be able to receive messages from other users so that I can chat with them.
- UC06: As a user, I want to be able to see my chat history so that I can see what I've talked about with other users.
- UC07: As a user, I want to be able to see my chat history with a specific user so that I can see what I've talked about with them.
- UC08: As a user, I want to be able to log out of my account so that I can stop using the app.*

*Security ist not part of the project, but it's still a use case.

## Use Case Diagram

![Use Case Diagram](./whatspoppin-usecasediagram.png)

## Sketches

![Sketch](./sketch.png)

## System Architecture

![System Architecture](./system-architecture.png)

## Functional Sketch

![Functional Sketch](./functional-sketch.png)

## Data Model

![Data Model](./data-model.png)

## API Documentation

### auth

#### login / register user

_This isn't a secure login. But it's irrelevant for this project._

<details>
  <summary>POST /auth</summary>

```
{
  username: string
}

4XX / 5XX
{
  error: string
}

200
{
  user_id: string,
  username: string
}
```
</details>

### users

#### query users

<details>
  <summary>GET /users?query={query}</summary>

```
4XX / 5XX
{
  error: string
}

200
[
  user_id: string,
  username: string
]
```
</details>

### history

#### get user history

<details>
  <summary>GET /history/{user_id}</summary>

```
4XX / 5XX
{
  error: string
}

200
[
  history_id: string,
  user_ids: []string,
  chat_id: string
]
```

</details>

### chats

#### get chat

<details>
  <summary>GET /chats/{chat_id}</summary>

```
4XX / 5XX
{
  error: string
}

200
{
  chat_id: string,
  user_ids: string[],
  messages: [
    message_id: string,
    author_id: string,
    message: string,
    sent_at: timestamp
  ]
}

```

</details>

#### create chat

<details>
  <summary>POST /chats</summary>

```
{
  user_ids: string[]
  initial_message: {
    author_id: string,
    message: string
  }
}

4XX / 5XX
{
  error: string
}

200
{
  history_id: string,
  chat_id: string
}
```
</details>

### messages

#### send message

<details>
  <summary>POST /messages</summary>

```
{
  chat_id: string,
  sender_id: string,
  message: string
}

4XX / 5XX
{
  error: string
}

200
{
  message_id: string
}
```
</details> 