# WhatsPoppin

WhatsPoppin is a web app that allows website visitors to chat with each other💬.

It's a school project to train our skills in web development. The security of the app is not part of the project.

## Planing

I wanted to make something like WhatsApp, so I called it "WhatsPoppin". It would let people send and get text messages easily, just like chatting with friends.

Read more about the planing [here](./planing/readMe.md).